<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 15:16
 */
include "document.php";
class Livre extends Document
{

    protected $nbPages;

    /**
     * Livre constructor.
     * @param $nbPages
     */
    public function __construct($reference, $nom, $date_creation, $auteur,$nbPages)
    {
        parent::__construct($reference,$nom,$date_creation,$auteur);
        $this->nbPages = $nbPages;
    }

    /**
     * @return mixed
     */

    public function getNbPages()
    {
        return $this->nbPages;
    }

    /**
     * @param mixed $nbPages
     */
    public function setNbPages($nbPages)
    {
        $this->nbPages = $nbPages;
    }

    /**
     * Livre constructor.
     * @param $nbPages
     */





}