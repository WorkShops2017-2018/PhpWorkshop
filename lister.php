<script>
    function supprimer(ref) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("tab").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", 'supprimer.php?ref=' + ref);
        xhttp.send();
    }
</script>
<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 16:22
 */

include "crudLivre.php";
$crudLivre = new crudLivre();
$liste = $crudLivre->consulterLivre();
echo "<div id='tab'><table border='1'>
    <tr>
        <td>Ref</td>
        <td>nom</td>
        <td>auteur</td>
        <td>nb pages</td>
        <td>date</td>
    </tr>
";
foreach ($liste as $item) {
    ?>
    <tr>
        <td><?php echo $item[0]; ?></td>
        <td><?php echo $item['Nom']; ?></td>
        <td><?php echo $item['Auteur']; ?></td>
        <td><?php echo $item['Nb_pages']; ?></td>
        <td><?php echo $item['Date_creation']; ?></td>
        <td>
            <button onclick="supprimer(<?php echo $item[0]; ?>)">Supprimer</button>
        </td>
    </tr>
    <?php
}
?>
</table>
</div>
