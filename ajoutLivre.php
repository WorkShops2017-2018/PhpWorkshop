<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 16:00
 */
include "crudLivre.php";
$nom=$_GET['nom'];
$reference=$_GET['ref'];
$date=$_GET['date'];
$nb=$_GET['nb'];
$auteur=$_GET['auteur'];

$livre = new Livre($reference,$nom,$date,$auteur,$nb);

$crud=new crudLivre() ;
$crud->ajouterLivre($livre);

header('Location: lister.php');