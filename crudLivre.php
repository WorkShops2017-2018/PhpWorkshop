<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 15:39
 */
include "config.php";
include "Livre.php";

class crudLivre
{

    public $conn;

    /**
     * crudLivre constructor.
     * @param $conn
     */
    public function __construct()
    {
        $c = new config();
        $c->connexion();
        $this->conn = $c->getConn();
    }

    function ajouterLivre($livre)
    {
        $sql = "INSERT INTO document (reference,nom,auteur,date_creation,Nb_pages,type) VALUES (:ref,:nom,:auteur,:date_cre,:nb,'livre')";
        $req = $this->conn->prepare($sql);
        $req->bindValue(":ref", $livre->getReference());
        $req->bindValue(":nom", $livre->getNom());
        $req->bindValue(":auteur", $livre->getAuteur());
        $req->bindValue(":date_cre", $livre->getDateCreation());
        $req->bindValue(":nb", $livre->getNbPages());
        $req->execute();
    }

    function consulterLivre()
    {
        $sql = "select * from document where type='livre'";
        $res=$this->conn->query($sql);
        return $res->fetchAll();
    }
    function supprimerLivre($ref)
    {
        $sql = "delete from document where Reference=:ref";
        $req = $this->conn->prepare($sql);
        $req->bindValue(":ref", $ref);
        $req->execute();
    }


}