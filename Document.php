<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 15:08
 */

class Document
{
    protected $reference;
    protected $nom;
    protected $date_creation;
    protected $auteur;

    /**
     * Document constructor.
     * @param $reference
     * @param $nom
     * @param $date_creation
     * @param $auteur
     */
    public function __construct($reference, $nom, $date_creation, $auteur)
    {
        $this->reference = $reference;
        $this->nom = $nom;
        $this->date_creation = $date_creation;
        $this->auteur = $auteur;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * @param mixed $date_creation
     */
    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    }






}