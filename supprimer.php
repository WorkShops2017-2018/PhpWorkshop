<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 16:56
 */

include "crudLivre.php";
$c = new crudLivre();
$c->supprimerLivre($_GET['ref']);
$liste = $c->consulterLivre();
echo "<table border='1'>
    <tr>
        <td>Ref</td>
        <td>nom</td>
        <td>auteur</td>
        <td>nb pages</td>
        <td>date</td>
    </tr>
";
foreach ($liste as $item) {
    ?>
    <tr>
        <td><?php echo $item[0]; ?></td>
        <td><?php echo $item['Nom']; ?></td>
        <td><?php echo $item['Auteur']; ?></td>
        <td><?php echo $item['Nb_pages']; ?></td>
        <td><?php echo $item['Date_creation']; ?></td>
        <td><button onclick="supprimer(<?php echo $item[0]; ?>)">Supprimer</button></td>
    </tr>
    <?php
}
?>
</table>
