<?php
/**
 * Created by PhpStorm.
 * User: Bassem
 * Date: 29/11/2017
 * Time: 15:26
 */

class config
{

    private $conn;

    public function connexion() {
         $servername="localhost";
         $username="root";
         $password="";
         $dbname="atelierphp";
         $conn=new PDO("mysql:host=$servername;dbname=$dbname",$username,$password);
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $this->conn=$conn;



    }

    public function getConn() {
        return $this->conn;
    }

}